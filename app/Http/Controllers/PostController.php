<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// access the authenticated user via the Auth Facades
use Illuminate\Support\Facades\Auth;
use App\Models\Post;


class PostController extends Controller
{
    //action to return a view containing a form for a blog post creation.
    public function create(){
        return view('posts.create');
    }

    /* 
        action to recieven form data and subsiquently store said data in the posts table.
    */
    public function store(Request $request){
       if(Auth::user()){
            $post = new Post;
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->user_id = (Auth::user()->id);

            $post->save();

            return redirect('/posts');
       }
       else{
            return redirect('/login');
       }
    }


    /* 
        action that will returnb a view showing all blog posts.
    */

    public function index(){
        $posts = Post::all();
        return view('posts.index')->with('posts', $posts);
    }

    //Activity for s02
    //action that will return a view showing random blog posts
    public function welcome()
    {
        $posts = Post::inRandomOrder()
                ->limit(3)
                ->get();
        /* return view('posts.index')->with('posts', $posts); */
        return view('welcome')->with('posts', $posts);

    }


    /* 
        Action for showing only the posts authored by authenticated user / currently logged in user
    */
    public function myPosts(){
        if(Auth::user()){
            $posts = Auth::user()->posts;
            return view('posts.index')->with('posts', $posts);
        }
        else{
            return redirect('/login');
        }
    }


    /* 
        action that will return a view showing a specific post using the URL parameter $id to query for the database entry to be shown

    */
    public function show($id){
        $posts = Post::find($id);

        return view('posts.show')->with('posts', $posts);
    }




    /* 
// s03 a1
    1. Define a route that will return an "edit form" for a specific Post when a GET request is received at the /posts/{id}/edit endpoint.
*/
    public function editForm()
    {
        if (Auth::user()) {
            $posts = Auth::user()->posts;
            return view('posts.index')->with('posts', $posts);
        } else {
            return redirect('/login');
        }
    }


    public function edit($id)
    {
        $posts = Post::find($id);

        return view('posts.edit')->with('posts', $posts);
    }

}
