

@extends('layouts.app')

@section('content')


    <div class="text-center">
        <img src="https://laravelnews.imgix.net/images/laravel-featured.png" alt="laravel link" class="w-50">
    

    <h2 class="my-4">
        Featured Posts:
    </h2>

 @if(count($posts) > 0)
        @foreach($posts as $post)
            <div class="card text-center">
                <div class="card-body">
                    <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                    <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
                    {{-- <p class="card-subtitle mb-3 text-muted">Created at: {{$post->created_at}}</p> --}}
                    {{-- check if there is an authenticated user to prevent our web app from throwing an error when no user is logged in --}}
                </div>
                @if(Auth::user())
                    {{-- if the authenticated user is the author of this blog post --}}
                    @if(Auth::user()->id == $post->user_id)
                        {{-- show an edit post button and a delete post button --}}
                        <div class="card-footer">
                            <form method="POST" action="/posts/{{$post->id}}">
                                @method('DELETE')
                                @csrf
                                <a href="/posts/{{$post->id}}/edit" class="btn btn-primary">Edit post</a>
                                <button type="submit" class="btn btn-danger">Delete Post</button>
                            </form>
                        </div>
                    @endif
                @endif
            </div>
        @endforeach
    @else
        <div>
            <h2>There are no posts to show</h2>
            <a href="/posts/create" class="btn btn-info">Create post</a>
        </div>
    @endif

    </div>
@endsection