@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <h2 class="card-title">
                {{$posts->title}}
            </h2>
                <p class="card-subtitle text-muted">
                    Author: {{$posts->user->name}}
                </p>
                 <p class="card-subtitle text-muted">
                    Created at: {{$posts->created_at}}
                </p>
                <p class="card-text">
                    Created at: {{$posts->content}}
                </p>
                <div class="mt-3">
                    <a href="/posts" class="card-link">
                        View All Posts
                    </a>
                </div>
            
        </div>
    </div>
@endsection